package main

import (
	"log"
	"net/http"
)

type handler struct {
}

func (h *handler) ServeHTTP(response http.ResponseWriter, request *http.Request) {
	_, err := response.Write([]byte("Hello"))
	if err != nil {
		panic(err.Error())
	}
}
func main() {
	h := handler{}
	server := http.Server{
		Addr:    ":33066",
		Handler: &h,
	}
	log.Fatal(server.ListenAndServe())
}
