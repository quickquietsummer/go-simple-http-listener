FROM golang:1.19

WORKDIR /usr/src/app

COPY go.mod ./

COPY . .

EXPOSE 33066

RUN go build -v -o /usr/local/bin/app ./...

CMD ["app"]